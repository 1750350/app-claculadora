 package com.example.calculadora;



import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

 public class MainActivity extends AppCompatActivity {
    private EditText txtNum1;
    private EditText txtNum2;
    private Button btnsumar;
    private Button btnrestar;
    private Button btnmultiplicar;
    private Button btndividir;
    private TextView lblresultado;
    private Calculadora Calculadora;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        txtNum1=(EditText) findViewById(R.id.txtNum1);
        txtNum2=(EditText) findViewById(R.id.txtNum2);

        btnsumar=(Button) findViewById(R.id.btnsumar);
        btnrestar=(Button) findViewById(R.id.btnrestar);
        btndividir=(Button) findViewById(R.id.btndividir);
        btnmultiplicar=(Button) findViewById(R.id.btnmultiplicar);

        lblresultado=(TextView) findViewById(R.id.lblresultado);

        Calculadora = new Calculadora(0.0f,0.0f);

        btnsumar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (txtNum1.getText().toString().matches("")||
                txtNum2.getText().toString().matches("")
                ){

                    Toast.makeText(MainActivity.this,"Faltó Capturar Número",
                            Toast.LENGTH_SHORT).show();

                }
                else {

                    float num1 = Float.parseFloat(txtNum1.getText().toString());
                    float num2 = Float.parseFloat(txtNum2.getText().toString());
                    Calculadora.setNum1(num1);
                    Calculadora.setNum2(num2);
                    float res = Calculadora.sumar();
                    lblresultado.setText(String.valueOf(res));
                }
            }
        });
      btnrestar.setOnClickListener(new View.OnClickListener() {
          @Override
          public void onClick(View v) {

              if (txtNum1.getText().toString().matches("")||
              txtNum2.getText().toString().matches("")
              ){
                  Toast.makeText(MainActivity.this,"Faltó Capturar Número",
                          Toast.LENGTH_SHORT).show();
              }
              else {
                  float num1 = Float.parseFloat(txtNum1.getText().toString());
                  float num2 = Float.parseFloat(txtNum2.getText().toString());
                  Calculadora.setNum1(num1);
                  Calculadora.setNum2(num2);
                  float res = Calculadora.restar();
                  lblresultado.setText(String.valueOf(res));
              }
          }
      });

      btnmultiplicar.setOnClickListener(new View.OnClickListener() {
          @Override
          public void onClick(View v) {

              if (txtNum1.getText().toString().matches("")||
              txtNum2.getText().toString().matches("")
              ){
                  Toast.makeText(MainActivity.this,"Faltó Capturar Número",
                          Toast.LENGTH_SHORT).show();

              }
              else {
                  float num1 = Float.parseFloat(txtNum1.getText().toString());
                  float num2 = Float.parseFloat(txtNum2.getText().toString());
                  Calculadora.setNum1(num1);
                  Calculadora.setNum2(num2);
                  float res = Calculadora.multiplicar();
                  lblresultado.setText(String.valueOf(res));
              }
          }
      });

      btndividir.setOnClickListener(new View.OnClickListener() {
          @Override
          public void onClick(View v) {
              if (txtNum1.getText().toString().matches("") ||
                      txtNum2.getText().toString().matches("")
              ) {
                  Toast.makeText(MainActivity.this, "Faltó Capturar Número",
                          Toast.LENGTH_SHORT).show();

              }

              else {
                  float num1 = Float.parseFloat(txtNum1.getText().toString());
                  float num2 = Float.parseFloat(txtNum2.getText().toString());
                  Calculadora.setNum1(num1);
                  Calculadora.setNum2(num2);
                  float res = Calculadora.division();
                  lblresultado.setText(String.valueOf(res));
              }
          }
      });
    }
}